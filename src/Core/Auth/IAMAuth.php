<?php

// Copyright 2024 yydick
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

namespace Yydick\Qianfan\Core\Auth;

use GuzzleHttp\Psr7\Request;
use Yydick\Qianfan\Interfaces\AuthInterface;

class IAMAuth implements AuthInterface
{
    public function __construct(protected readonly string $accessKey = "", protected readonly string $secretKey = "")
    {
        if (empty($this->accessKey)) {
            $this->accessKey = config("qianfan.access_key");
        }
        if (empty($this->secretKey)) {
            $this->secretKey = config("qianfan.secret_key");
        }
    }
    public function signRequest(Request $request): Request
    {
        return $request;
    }

    public function sign(): string
    {
        $url = "";
        return $url;
    }
}
