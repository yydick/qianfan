<?php

// Copyright 2024 yydick
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

namespace Yydick\Qianfan\Core\Auth;

use Yydick\Qianfan\Core\Auth\IAMAuth;
use Yydick\Qianfan\Core\Auth\QianfanOAuth;
use Yydick\Qianfan\Interfaces\AuthInterface;

class Auth
{
    public const TYPE_IAM = "IAM";
    public const TYPE_OAUTH = "OAuth";

    private function __construct()
    {
    }

    public static function createIAMAuth(string $accessKey, string $secretKey): AuthInterface
    {
        return new IAMAuth($accessKey, $secretKey);
    }
    public static function createOAuth(string $qianfanAK, string $qianfanSK): AuthInterface
    {
        return new QianfanOAuth($qianfanAK, $qianfanSK);

    }
}
